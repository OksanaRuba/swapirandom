import React, { useContext, useState, useEffect } from 'react';
import { NameContext } from '../app/app';
import CircularProgress from '@material-ui/core/CircularProgress';
import Service from '../../services/service';

import './random-planet.css';

const RandomPlanet = () => {
  const [information, setInformation] = useState(0)
  const [image, setImage] = useState(null)

  const { profileInformation } = useContext(NameContext);

  useEffect(() => {
    setInformation(profileInformation)
    if (profileInformation !== null) {
      const symbol = profileInformation.url.match(/\d+/)
      new Service()//екземпляр класа
        .getPlanetsImage(symbol)
        .then((url) => {
          setImage(0)
        })
    }
  }, [profileInformation])

  /*useEffect(() => {
    setImage(0)
    const interval = setInterval(1000);

    return () => clearInterval(interval)
  }, [image]);
*/
  if (information) {
    return (
      <div className="random-planet jumbotron rounded">
        {image === null ? <CircularProgress /> :
          <img className="planet-image"
            src={image} />}
        <div>
          <h4>Planet Name</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <span className="term">Population</span>
              <span>{information.population}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Rotation Period</span>
              <span>{information.rotation_period}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Diameter</span>
              <span>{information.diameter}</span>
            </li>
          </ul>
        </div>
      </div>
    );
  } else {
    return (<div></div>)
  }
}

export default RandomPlanet;