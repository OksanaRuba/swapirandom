import React, { useContext, useState, useEffect } from 'react';
import { NameContext } from '../app/app';
import CircularProgress from '@material-ui/core/CircularProgress';

import Service from '../../services/service';

import './person-details.css';

const PersonDetails = () => {
  const [information, setInformation] = useState(null)
  const [image, setImage] = useState(null)

  const { profileInformation } = useContext(NameContext);

  useEffect(() => {
    setInformation(profileInformation)

    if (profileInformation !== null) {
      const symbol = profileInformation.url.match(/\d+/)
      const typeSymbol = profileInformation.url.split('/')
      switch (typeSymbol[4]) {
        case 'people':
          new Service()
            .getPeopleImage(symbol)
            .then((url) => setImage(url));
          break;
        case 'planets':
          new Service()
            .getPlanetsImage(symbol)
            .then((url) => setImage(url));
          break;
        case 'starships':
          new Service()
            .getStarshipsImage(symbol)
            .then((url) => setImage(url));
          break;
      }

    }
  return () => {setImage(null)}}, [profileInformation])

  console.log(typeof image);

  if (information) {
    return (
      <div className="person-details card">
        {image === null ? <CircularProgress /> :
          <img className="person-image"
            src={image} />}

        <div className="card-body">
          <h4>{information.name}</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              {information.gender ?
                <div>
                  <span className="term">Gender</span>
                  <span>{information.gender}</span>
                </div>
                : information.climate ?
                  <div>
                    <span className="term">Climate</span>
                    <span>{information.climate}</span>
                  </div>
                  :
                  <div>
                    <span className="term">Crew</span>
                    <span>{information.crew}</span>
                  </div>
              }
            </li>
            <li className="list-group-item">
              {information.birth_year ?
                <div>
                  <span className="term">Birth Year</span>
                  <span>{information.birth_year}</span>
                </div>
                : information.gravity ?
                  <div>
                    <span className="term">Gravity</span>
                    <span>{information.gravity}</span>
                  </div>
                  :
                  <div>
                    <span className="term">Length</span>
                    <span>{information.length}</span>
                  </div>
              }
            </li>
            <li className="list-group-item">
              {information.eye_color ?
                <div>
                  <span className="term">Eye color</span>
                  <span>{information.eye_color}</span>
                </div>
                : information.surface_water ?
                  <div>
                    <span className="term">Surface water</span>
                    <span>{information.surface_water}</span>
                  </div>
                  :
                  <div>
                    <span className="term">Model</span>
                    <span>{information.model}</span>
                  </div>
              }
            </li>
          </ul>
        </div>
      </div>
    )
  } else {
    return (<div></div>)
  }
}

export default PersonDetails;