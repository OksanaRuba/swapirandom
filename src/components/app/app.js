import React, { createContext, useState, useEffect } from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ItemList from '../item-list';
import PersonDetails from '../person-details';
import Service from '../../services/service';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";


import './app.css';

const NameContext = createContext();
const PersonContext = createContext();

const App = () => {

  const [profileInformation, setProfileInformation] = useState(null);

  useEffect(() => {
  })

  const onElementInfo = (value) => {
    setProfileInformation(value);
    
  }

  return (
    <Router>
      <NameContext.Provider value={{onElementInfo: onElementInfo, profileInformation: profileInformation}}>
        <Header />
        <RandomPlanet />
        <div className="row mb2">
          <Route exact path='/people'>
            <div className="col-md-6">
              <ItemList request={new Service().getPeoples()} />
            </div>
          </Route>
          <Route exact path='/planets'>
            <div className="col-md-6">
              <ItemList request={new Service().getPlanets()} />
            </div>
          </Route>
          <Route exact path='/starships'>
            <div className="col-md-6">
              <ItemList request={new Service().getStarships()} />
            </div>
          </Route>
          <div className="col-md-6">
            <PersonContext.Provider value={profileInformation}>
              <PersonDetails /> 
            </PersonContext.Provider>
          </div>
        </div>
      </NameContext.Provider>
    </Router>
  );
};

export { NameContext, PersonContext};
export default App;